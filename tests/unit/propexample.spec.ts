import {mount} from '@vue/test-utils'
import PropExample from "@/components/PropExample.vue"

describe('PropExample',()=>{
    it('The component receive and show title and content props',()=>{
        const wrapper=mount(PropExample,{
            props:{
                title:'Hello world',
                content:'Loren ipsum'
            }
        })
        if(expect(wrapper.find('h3').exists())){
            expect(wrapper.find('h3').text()).toBe('Hello world')
        }
        if(expect(wrapper.find('p').exists())){
            expect(wrapper.find('p').text()).toBe('Loren ipsum')
        }
    }),
    it('The component emmits the counter value',async()=>{
        const wrapper=mount(PropExample,{
            props:{
                title:'Hello world',
                content:'Loren ipsum'
            }
        })
        await wrapper.find('button').trigger('click')
        expect(wrapper.emitted().clickMe[0][0]).toBe(1)
    })
})